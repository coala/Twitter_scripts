# Quick script to download all asciinema URLs from @coala_analyzer
import tweepy
import os

#Twitter API credentials
consumer_token = os.environ['consumer_token']
consumer_secret = os.environ['consumer_secret']
access_token = os.environ['access_token']
access_secret = os.environ['access_secret']


def get_all_tweets(screen_name):
	#Twitter only allows access to the latest 3240 tweets.
	auth = tweepy.OAuthHandler(consumer_token, consumer_secret)
	auth.set_access_token(access_token, access_secret)
	api = tweepy.API(auth)

	coala_asciinema_urls = {}
	coala_alltweets=[]

	#make initial request for most recent tweets (200 is the maximum allowed
	#count in one request.)
	new_tweets = api.user_timeline(screen_name = screen_name,count=200)
	#save most recent tweets
	coala_alltweets.extend(new_tweets)
	#save the id of the oldest tweet less one
	oldest = coala_alltweets[-1].id - 1
	#keep grabbing tweets until there are no tweets left to grab
	while len(new_tweets) > 0:
		#all subsiquent requests use the max_id param to prevent duplicates
		new_tweets = api.user_timeline(screen_name = screen_name,count=200,
									   max_id=oldest)
		#save most recent tweets
		coala_alltweets.extend(new_tweets)
		#update the id of the oldest tweet less one
		oldest = coala_alltweets[-1].id - 1

	for tweets in coala_alltweets:
		for url in tweets.entities['urls']:
			if "asciinema" in url['expanded_url']:
				coala_asciinema_urls[url['expanded_url']]=tweets.text

	return coala_asciinema_urls

if __name__ == '__main__':
	#pass in the username of the account you want to download
	print(get_all_tweets("coala_analyzer"))
